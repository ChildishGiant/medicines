/* window.vala
 *
 * Copyright 2023 Allie Law
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Medicines {
    [GtkTemplate (ui = "/page/allie/medicines/window.ui")]
    public class Window : Adw.ApplicationWindow {

        [GtkChild]
        private unowned Adw.PreferencesGroup meds;

        public Window (Gtk.Application app) {
            Object (application: app);
        }

        public void init () {
            var settings = new Settings ("page.allie.medicines.state");

            // update the settings when the properties change and vice versa
            settings.bind ("width", this,
                           "default-width", SettingsBindFlags.DEFAULT);
            settings.bind ("height", this,
                           "default-height", SettingsBindFlags.DEFAULT);
            settings.bind ("is-maximized", this,
                           "maximized", SettingsBindFlags.DEFAULT);
            settings.bind ("is-fullscreen", this,
                           "fullscreened", SettingsBindFlags.DEFAULT);

        }

        construct {


        }
    }
}

