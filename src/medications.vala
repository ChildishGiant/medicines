/* medicine.vala
 *
 * Copyright 2023 Allie Law
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Model for a dosage
public class Medicines.Dosage : Object {
    public const string[] POSSIBLE_UNITS = {"mg","ml"};
    public string unit {get; set; default = "mg"; }
    public int remaining {get; set; default = -1; }

}

// Model for a pill
public class Medicines.Medications : GLib.Object {
    public string name {get; set; }

    // Hashmap of pill sizes and remaining, -1 for not counted
    public GLib.HashTable<float?, Medicines.Dosage> stock {get; set;}

    // Serialize to file
    public void store () {
        // Try to load file from store
        // DATA_DIR
    }
    // 50mgs - 10 left
    // 2000ml left
}
